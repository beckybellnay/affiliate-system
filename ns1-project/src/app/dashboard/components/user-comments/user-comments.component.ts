import { Component, OnInit, Input } from '@angular/core';
import { UserComment } from '../../models/user-comment.model';

@Component({
  selector: 'app-user-comments',
  templateUrl: './user-comments.component.html',
  styleUrls: ['./user-comments.component.scss'],
})
export class UserCommentsComponent implements OnInit {
  @Input()
  userComments: UserComment[];

  constructor() {}

  ngOnInit(): void {}
}
