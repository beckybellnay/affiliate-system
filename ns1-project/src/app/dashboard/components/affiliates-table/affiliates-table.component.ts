import { Component, OnInit, Input } from '@angular/core';
import { Affiliate } from '../../models/affiliate.model';

@Component({
  selector: 'app-affiliates-table',
  templateUrl: './affiliates-table.component.html',
  styleUrls: ['./affiliates-table.component.scss'],
})
export class AffiliatesTableComponent implements OnInit {
  @Input()
  affiliates: Affiliate[];

  constructor() {}

  ngOnInit(): void {}

  sortName = (a: Affiliate, b: Affiliate) => a.name.localeCompare(b.name);

  sortPartnershipDate = (a: Affiliate, b: Affiliate) =>
    a.partnershipDate.localeCompare(b.partnershipDate);

  sortUsers = (a: Affiliate, b: Affiliate) => a.users - b.users;

  sortRefusedUsers = (a: Affiliate, b: Affiliate) =>
    a.refusedUsersPerMonth - b.refusedUsersPerMonth;

  sortNewUsersPerMonth = (a: Affiliate, b: Affiliate) =>
    a.newUsersPerMonth - b.newUsersPerMonth;

  sortProfits = (a: Affiliate, b: Affiliate) =>
    a.profitsPerMonth - b.profitsPerMonth;

  sortCommision = (a: Affiliate, b: Affiliate) => a.commision - b.commision;
}
