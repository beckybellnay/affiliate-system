import { Component, OnInit, Input } from '@angular/core';
import { Affiliate } from '../../models/affiliate.model';

@Component({
  selector: 'app-affiliates',
  templateUrl: './affiliates.component.html',
  styleUrls: ['./affiliates.component.scss'],
})
export class AffiliatesComponent implements OnInit {
  @Input()
  affiliates: Affiliate[];

  constructor() {}

  ngOnInit(): void {}
}
