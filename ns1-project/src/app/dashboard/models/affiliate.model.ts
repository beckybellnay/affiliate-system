export class Affiliate {
  name: string;
  partnershipDate: string;
  users: number;
  refusedUsersPerMonth: number;
  newUsersPerMonth: number;
  profitsPerMonth: number;
  commision: number;
}
