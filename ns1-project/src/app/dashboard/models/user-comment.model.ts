export class UserComment {
  name: string;
  date: string;
  description: string;
  vote: number;
}
