import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { CommonModule } from '@angular/common';
import { AffiliatesComponent } from './components/affiliates/affiliates.component';
import { NzZorroModule } from '../shared/nz-zorro.module';
import { AffiliatesTableComponent } from './components/affiliates-table/affiliates-table.component';
import { UserCommentsComponent } from './components/user-comments/user-comments.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DashboardComponent,
    AffiliatesComponent,
    AffiliatesTableComponent,
    UserCommentsComponent,
  ],
  imports: [CommonModule, FormsModule, DashboardRoutingModule, NzZorroModule],
  exports: [],
  providers: [],
})
export class DashboardModule {}
