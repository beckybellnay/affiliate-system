import { Component, OnInit } from '@angular/core';
import { Affiliate } from './models/affiliate.model';
import { UserComment } from './models/user-comment.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  mockAffilates: Affiliate[] = [
    {
      name: 'Remedium',
      partnershipDate: '2015-03-20',
      users: 25,
      refusedUsersPerMonth: 2,
      newUsersPerMonth: 4,
      profitsPerMonth: 2000,
      commision: 177.45,
    },
    {
      name: 'Medea',
      partnershipDate: '2016-04-21',
      users: 20,
      refusedUsersPerMonth: 8,
      newUsersPerMonth: 18,
      profitsPerMonth: 1500,
      commision: 1784.4,
    },
    {
      name: 'Subra',
      partnershipDate: '2010-04-28',
      users: 500,
      refusedUsersPerMonth: 2,
      newUsersPerMonth: 7,
      profitsPerMonth: 80000,
      commision: 255.8,
    },
    {
      name: 'SOpharmacy',
      partnershipDate: '2020-04-17',
      users: 10,
      refusedUsersPerMonth: 0,
      newUsersPerMonth: 0,
      profitsPerMonth: 0,
      commision: 0,
    },
    {
      name: 'Framar',
      partnershipDate: '2018-05-17',
      users: 100,
      refusedUsersPerMonth: 7,
      newUsersPerMonth: 80,
      profitsPerMonth: 5000,
      commision: 20.7,
    },
    {
      name: 'Vitania',
      partnershipDate: '2015-08-11',
      users: 545,
      refusedUsersPerMonth: 90,
      newUsersPerMonth: 40,
      profitsPerMonth: 3000,
      commision: 1555.8,
    },
    {
      name: 'Marvi',
      partnershipDate: '2016-12-18',
      users: 1770,
      refusedUsersPerMonth: 25,
      newUsersPerMonth: 100,
      profitsPerMonth: 2500,
      commision: 548.9,
    },
    {
      name: '36.6',
      partnershipDate: '2017-12-14',
      users: 447,
      refusedUsersPerMonth: 1,
      newUsersPerMonth: 1,
      profitsPerMonth: 50,
      commision: 500,
    },
    {
      name: 'Propharma',
      partnershipDate: '2017-12-25',
      users: 841,
      refusedUsersPerMonth: 63,
      newUsersPerMonth: 77,
      profitsPerMonth: 3247,
      commision: 280,
    },
    {
      name: 'Mareshki',
      partnershipDate: '2007-03-29',
      users: 8417,
      refusedUsersPerMonth: 0,
      newUsersPerMonth: 100,
      profitsPerMonth: 8000,
      commision: 700,
    },
    {
      name: 'Pharma Nova',
      partnershipDate: '2009-12-16',
      users: 1000,
      refusedUsersPerMonth: 4,
      newUsersPerMonth: 40,
      profitsPerMonth: 60000,
      commision: 750,
    },
    {
      name: 'Evridika',
      partnershipDate: '2016-06-24',
      users: 264,
      refusedUsersPerMonth: 4,
      newUsersPerMonth: 10,
      profitsPerMonth: 2781,
      commision: 300,
    },
  ];

  mockUserComments: UserComment[] = [
    {
      name: 'Alexander Spassov',
      date: '2020-07-30',
      description:
        'Изключително съм доволен от обслужването и бързата доставка!',
      vote: 5,
    },
    {
      name: 'Nikolay Bonev',
      date: '2020-05-25',
      description:
        'Чаках 10 дни продуктите си и накрая получих пратка на друг потребител, не бих поръчал отново от Медеа',
      vote: 1,
    },
    {
      name: 'Marissa Kamenowa',
      date: '2020-07-21',
      description:
        'Доставката беше бърза, но единия от продуктите беше с различно количество, от това което бях поръчала.',
      vote: 2,
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
