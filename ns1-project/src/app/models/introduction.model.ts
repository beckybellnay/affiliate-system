export class CompanyIntroduction {
  title: string;
  description: string;
  advantages: { title: string; description: string; img: string }[];
}
