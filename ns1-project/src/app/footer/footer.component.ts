import { Component, OnInit, Input } from '@angular/core';
import { Service } from '../models/service.model';
import { CustomerSupport } from '../models/customer-support.model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input()
  hostingServices: Service[];

  @Input()
  appHosting: Service[];

  @Input()
  additionalServices: Service[];

  @Input()
  customerInfo: CustomerSupport;

  constructor() {}

  ngOnInit(): void {}
}
