import { Component } from '@angular/core';
import { Service } from './models/service.model';
import { CustomerSupport } from './models/customer-support.model';
import { User } from './models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isCollapsed = false;
  hostingServices: Service[] = [
    { name: 'Web Hosting' },
    { name: 'Reseller Hosting' },
    { name: 'Managed Cloud VPS' },
    { name: 'Business Email Hosting' },
    { name: 'Self-managed Cloud VPS' },
  ];

  appHosting: Service[] = [
    { name: 'Drupal Hosting' },
    { name: 'Joomla Hosting' },
    { name: 'Moodle Hosting' },
    { name: 'Magento Hosting' },
    { name: 'WordPress Hosting' },
    { name: 'Prestashop Hosting' },
    { name: 'WooCommerce Hosting' },
  ];

  additionalServices: Service[] = [
    { name: 'SSL Certificates' },
    { name: 'Domain Names' },
    { name: 'Affiliate Program' },
    { name: 'Domain Reseller Account' },
  ];

  customerSupport: CustomerSupport = {
    contacts: 'Contact Us',
    clientAria: 'Client Area',
    knowledge: 'Knowledge base',
  };

  adminInfo: User = {
    name: 'Dolores Abernathy',
    role: 'Admin',
  };
}
