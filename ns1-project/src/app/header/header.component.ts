import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  activePage: 'home' | 'dashboard';
  constructor(private router: Router) {}

  ngOnInit(): void {}

  public navigateToDashboard(path: string): void {
    this.router.navigate([path]);
    this.activePage = 'dashboard';
  }

  public navigateToHome(path: string): void {
    this.router.navigate([path]);
    this.activePage = 'home';
  }
}
