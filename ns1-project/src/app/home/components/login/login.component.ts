import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  @Output()
  isSubmitted: EventEmitter<boolean> = new EventEmitter();

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  submitForm(form): void {
    if (form.valid) {
      console.log(form.value);
      form.resetForm();
      this.isSubmitted.emit(true);
    }
  }
}
