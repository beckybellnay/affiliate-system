import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss'],
})
export class ApplicationComponent implements OnInit {
  firstAplicationForm: FormGroup;
  secondApplicationForm: FormGroup;

  applicationStep: 'firstStep' | 'secondStep' | 'thirdStep' = 'firstStep';

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.firstAplicationForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
    });

    this.secondApplicationForm = this.fb.group({
      website: ['', Validators.required],
      description: ['', Validators.required],
      payPal: ['', [Validators.required, Validators.email]],
    });
  }

  submitFirstApplicationStep(form): void {
    if (form.valid) {
      this.applicationStep = 'secondStep';
    }
  }

  submitSecondApplicationStep(form): void {
    if (form.valid) {
      console.log({ ...this.firstAplicationForm.value, ...form.value });
      this.applicationStep = 'thirdStep';
      form.resetForm();
    }
  }

  onClickPreviousBtn(): void {
    this.applicationStep = 'firstStep';
  }
}
