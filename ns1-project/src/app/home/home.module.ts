import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApplicationComponent } from './components/application/application.component';
import { NzZorroModule } from '../shared/nz-zorro.module';

@NgModule({
  declarations: [HomeComponent, LoginComponent, ApplicationComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NzZorroModule,
  ],
})
export class HomeModule {}
