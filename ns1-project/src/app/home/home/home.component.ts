import { Component, OnInit } from '@angular/core';
import { CompanyIntroduction } from '../../models/introduction.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  showModalLogin = false;
  showModalApplication = false;
  companyIntro: CompanyIntroduction = {
    title: 'Join the Highest Paying Hosting Affiliate Program',
    description:
      'Help us spread the word about the next step in the hosting evolution, and earn up to $200 per sale! You get to promote the very best in hosting, while earning the industry"s highest',
    advantages: [
      {
        title: 'Industry-leading Product',
        description:
          'Thans to our R&D, our clients get unmathed performance, security and prices.',
        img: './assets/affiliate-images/r&d.png',
      },
      {
        title: 'Highest Paying Commision',
        description:
          'We"re fully independant, so we can offer the industry"s highest commision.',
        img: './assets/affiliate-images/percentage.png',
      },
      {
        title: 'Industry-leading Retention',
        description:
          '99%+ of our customers are happy, and each stays with us an avarage of 7 years.',
        img: './assets/affiliate-images/retention.png',
      },
      {
        title: 'Dedicated Affiliate Support',
        description:
          'Dedicated managers and resources to help you maximize your earnings.',
        img: './assets/affiliate-images/support.png',
      },
    ],
  };

  constructor() {}

  ngOnInit(): void {}

  onClickLogin(): void {
    this.showModalLogin = true;
  }

  onClickCloseLogin(): void {
    this.showModalLogin = false;
  }

  onClickApplication(): void {
    this.showModalApplication = true;
  }

  onClickCloseApplication(): void {
    this.showModalApplication = false;
  }
}
