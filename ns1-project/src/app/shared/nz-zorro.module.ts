import { NgModule } from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzRateModule } from 'ng-zorro-antd/rate';

const ngZorroModules = [
  NzLayoutModule,
  NzMenuModule,
  NzCarouselModule,
  NzRateModule,
  NzIconModule,
  NzFormModule,
  NzSelectModule,
  NzTableModule,
  NzCardModule,
  NzDescriptionsModule,
  NzDividerModule,
];

@NgModule({
  declarations: [],
  imports: [...ngZorroModules],
  exports: [...ngZorroModules],
})
export class NzZorroModule {}
